import java.util.Random;
public class RouletteWheel {
private Random rd;
private int number;
// constructor
public RouletteWheel(){
    rd = new Random();
    this.number = 0;
}

public boolean spin(int numberBet){
    this.number = rd.nextInt(37);
    if (this.number == numberBet){
        return true;
    }
    else{
        return false;
    }
}

public int getValue(){
    return number;
}
}